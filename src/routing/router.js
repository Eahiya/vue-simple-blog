import VueRouter from "vue-router";

import addBlog from '../components/add-blog'
import showBlog from '../components/show-blog'

import singleBlog from '../components/single-blog'



const routes = [
  {path: '/',  component: showBlog},
  {path: '/add-blog', component: addBlog},
  {path: '/blog/:id', component: singleBlog}
]

const router = new VueRouter({
  routes, // short for `routes: routes`
  mode: 'history'
})
export default router


