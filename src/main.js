import Vue from 'vue'
import App from './App.vue'

import VueResource from 'vue-resource'
Vue.use(VueResource);

import VueRouter from "vue-router";
Vue.use(VueRouter)

import router from "./routing/router";

// custom directive
Vue.directive('raincolor', {
  bind(el,binding,vNode)
  {
    el.style.color = '#' + Math.random().toString().slice(2,8)
  }
})

Vue.directive('theme', {
  bind: function (el, binding, vNode) {
    if (binding.value === 'wide') {
        el.style.maxWidth = '1200px'
    }else if (binding.value === 'narrow')
    {
      el.style.maxWidth = '560px'
    }
    if (binding.arg === 'column')
    {
      el.style.background = '#ddd'
      el.style.textAlign = 'left'
    }
  }
});

// make a filter
Vue.filter('uppercase', function (value) {
    return value.toUpperCase();
})

Vue.filter('lowercase', function (value) {
    return value.toLowerCase()
})

Vue.filter('slice', function (value) {
    return value.slice(0,100) + '...';
})


new Vue({
  el: '#app',
  render: h => h(App),
  router
})
